import esphome.codegen as cg
import esphome.config_validation as cv
from esphome import automation, pins
from esphome.components import i2c
from esphome.const import (
    CONF_ON_TAG,
    CONF_ON_TAG_REMOVED,
    CONF_TRIGGER_ID,
    CONF_RESET_PIN,
    CONF_GAIN,
)

CODEOWNERS = ["@glmnet"]
AUTO_LOAD = ["binary_sensor"]

CONF_RC522_ADV_ID = "rc522_adv_id"

rc522_adv_ns = cg.esphome_ns.namespace("rc522_adv")
RC522_ADV = rc522_adv_ns.class_("RC522", cg.PollingComponent, i2c.I2CDevice)
RC522AdvTrigger = rc522_adv_ns.class_(
    "RC522Trigger", automation.Trigger.template(cg.std_string)
)

RC522AdvGain = rc522_adv_ns.enum("RC522AdvGain")
GAIN = {
    "18DB": RC522AdvGain.RC522_GAIN_18DB,
    "23DB": RC522AdvGain.RC522_GAIN_23DB,
    "18DB_ALT": RC522AdvGain.RC522_GAIN_18DB_2,
    "23DB_ALT": RC522AdvGain.RC522_GAIN_23DB_2,
    "33DB": RC522AdvGain.RC522_GAIN_33DB,
    "38DB": RC522AdvGain.RC522_GAIN_38DB,
    "43DB": RC522AdvGain.RC522_GAIN_43DB,
    "48DB": RC522AdvGain.RC522_GAIN_48DB,
    "MIN": RC522AdvGain.RC522_GAIN_MIN,
    "AVG": RC522AdvGain.RC522_GAIN_AVG,
    "MAX": RC522AdvGain.RC522_GAIN_MAX,
    "MINIMUM": RC522AdvGain.RC522_GAIN_MIN,
    "AVERAGE": RC522AdvGain.RC522_GAIN_AVG,
    "MAXIMUM": RC522AdvGain.RC522_GAIN_MAX,
}

RC522_ADV_SCHEMA = cv.Schema(
    {
        cv.GenerateID(): cv.declare_id(RC522_ADV),
        cv.Optional(CONF_RESET_PIN): pins.gpio_output_pin_schema,
        cv.Optional(CONF_ON_TAG): automation.validate_automation(
            {
                cv.GenerateID(CONF_TRIGGER_ID): cv.declare_id(RC522AdvTrigger),
            }
        ),
        cv.Optional(CONF_ON_TAG_REMOVED): automation.validate_automation(
            {
                cv.GenerateID(CONF_TRIGGER_ID): cv.declare_id(RC522AdvTrigger),
            }
        ),
        cv.Optional(CONF_GAIN, default="33DB"): cv.enum(GAIN, upper=True),
    }
).extend(cv.polling_component_schema("1s"))

async def setup_rc522(var, config):
    await cg.register_component(var, config)

    if CONF_RESET_PIN in config:
        reset = await cg.gpio_pin_expression(config[CONF_RESET_PIN])
        cg.add(var.set_reset_pin(reset))

    for conf in config.get(CONF_ON_TAG, []):
        trigger = cg.new_Pvariable(conf[CONF_TRIGGER_ID])
        cg.add(var.register_ontag_trigger(trigger))
        await automation.build_automation(trigger, [(cg.std_string, "x")], conf)

    for conf in config.get(CONF_ON_TAG_REMOVED, []):
        trigger = cg.new_Pvariable(conf[CONF_TRIGGER_ID])
        cg.add(var.register_ontagremoved_trigger(trigger))
        await automation.build_automation(trigger, [(cg.std_string, "x")], conf)

    cg.add(var.set_gain(config[CONF_GAIN]))