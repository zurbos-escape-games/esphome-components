import esphome.codegen as cg
import esphome.config_validation as cv
from esphome.components import sensor
from esphome.const import CONF_LEVEL
from . import rc522_adv_ns, RC522_ADV, CONF_RC522_ADV_ID

DEPENDENCIES = ["rc522_adv"]

RC522AdvSensor = rc522_adv_ns.class_("RC522ErrorSensor", sensor.Sensor)

RC522AdvELvl = RC522AdvSensor.enum("ErrLevel")
LEVEL = {
    "WARN": RC522AdvELvl.WARN,
    "ERR": RC522AdvELvl.ERR,
    "WARNING": RC522AdvELvl.WARN,
    "ERROR": RC522AdvELvl.ERR,
}

CONFIG_SCHEMA = sensor.sensor_schema(RC522AdvSensor).extend(
    {
        cv.GenerateID(CONF_RC522_ADV_ID): cv.use_id(RC522_ADV),
        cv.Optional(CONF_LEVEL, default="WARN"): cv.enum(LEVEL, upper=True),
    }
)

async def to_code(config):
    var = await sensor.new_sensor(config)

    hub = await cg.get_variable(config[CONF_RC522_ADV_ID])
    cg.add(hub.register_err(var))
    cg.add(var.set_level(config[CONF_LEVEL]))
