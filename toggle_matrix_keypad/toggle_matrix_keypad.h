#pragma once

#include "esphome/core/component.h"
#include "esphome/core/hal.h"
#include "esphome/core/automation.h"
#include <cstdlib>
#include <utility>

namespace esphome {
namespace toggle_matrix_keypad {

class ToggleMatrixKeypadListener {
 public:
  virtual void button_pressed(int row, int col){};
  virtual void button_released(int row, int col){};
  virtual void key_pressed(uint8_t key){};
  virtual void key_released(uint8_t key){};
};

class ToggleMatrixKeypad : public Component {
 public:
  ToggleMatrixKeypad();
  void setup() override;
  void loop() override;
  void dump_config() override;
  void set_columns(std::vector<GPIOPin *> pins) { columns_ = std::move(pins); };
  void set_rows(std::vector<GPIOPin *> pins) { rows_ = std::move(pins); };
  void set_keys(std::string keys) { keys_ = std::move(keys); };
  void set_debounce_time(int debounce_time) { debounce_time_ = debounce_time; };
  void set_has_pulldowns(int has_pulldowns) { has_pulldowns_ = has_pulldowns; };
  void set_long_wire(bool long_wire) { long_wire_ = long_wire; };
  Trigger<std::string> *get_progress_trigger() const { return this->progress_trigger_; };


  void register_listener(ToggleMatrixKeypadListener *listener);

 protected:
  bool *pressed_keys_{nullptr};
  bool *active_keys_{nullptr};
  uint32_t *active_start_{nullptr};
  std::vector<GPIOPin *> rows_;
  std::vector<GPIOPin *> columns_;
  std::string keys_;
  int debounce_time_ = 0;
  bool has_pulldowns_{false};
  bool long_wire_{false};
  Trigger<std::string> *progress_trigger_;

  std::vector<ToggleMatrixKeypadListener *> listeners_{};
};

}  // namespace matrix_keypad
}  // namespace esphome