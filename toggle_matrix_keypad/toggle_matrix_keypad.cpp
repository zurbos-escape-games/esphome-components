#include "toggle_matrix_keypad.h"
#include "esphome/core/log.h"
#include "esphome/core/helpers.h"

namespace esphome {
namespace toggle_matrix_keypad {

static const char *const TAG = "toggle_matrix_keypad";

ToggleMatrixKeypad::ToggleMatrixKeypad()
    : progress_trigger_(new Trigger<std::string>()) {}

void ToggleMatrixKeypad::setup() {
  size_t len = this->rows_.size() * this->columns_.size();
  for (auto *pin : this->rows_) {
    pin->pin_mode(gpio::FLAG_OUTPUT);
    pin->digital_write(has_pulldowns_);
  }
  for (auto *pin : this->columns_) {
    if (has_pulldowns_) {
      pin->pin_mode(gpio::FLAG_INPUT);
    } else {
      pin->pin_mode(gpio::FLAG_INPUT | gpio::FLAG_PULLUP);
    }
  }
  ExternalRAMAllocator<bool> allocator_b(ExternalRAMAllocator<bool>::ALLOW_FAILURE);
  ExternalRAMAllocator<uint32_t> allocator_u32(ExternalRAMAllocator<uint32_t>::ALLOW_FAILURE);
  this->pressed_keys_ = allocator_b.allocate(len);
  if (this->pressed_keys_ == nullptr) {
    ESP_LOGE(TAG, "Failed to allocate key buffer of size %u", len);
    this->mark_failed();
    return;
  }
  memset(this->pressed_keys_, false, len * sizeof(bool));
  this->active_keys_ = allocator_b.allocate(len);
  if (this->active_keys_ == nullptr) {
    ESP_LOGE(TAG, "Failed to allocate key buffer of size %u", len);
    this->mark_failed();
    return;
  }
  memset(this->active_keys_, false, len * sizeof(bool));
  this->active_start_ = allocator_u32.allocate(len);
  if (this->active_start_ == nullptr) {
    ESP_LOGE(TAG, "Failed to allocate key buffer of size %u", len);
    this->mark_failed();
    return;
  }
  memset(this->active_start_, 0, len * sizeof(uint32_t));
}

void ToggleMatrixKeypad::loop() {
  uint32_t now = millis();
  int pos = 0, key_row, key_col;
  bool progress = false;
  for (auto *row : this->rows_) {
    row->digital_write(has_pulldowns_);
    if (this->long_wire_)
      delay(1);
    for (auto *col : this->columns_) {
      bool active = (col->digital_read() == has_pulldowns_);
      key_row = pos / this->columns_.size();
      key_col = pos % this->columns_.size();
      if (!active && this->pressed_keys_[pos]) {
        ESP_LOGD(TAG, "key @ row %d, col %d released", key_row, key_col);
        for (auto &listener : this->listeners_)
          listener->button_released(key_row, key_col);
        if (!this->keys_.empty()) {
          uint8_t keycode = this->keys_[pos];
          ESP_LOGD(TAG, "key '%c' released", keycode);
          for (auto &listener : this->listeners_)
            listener->key_released(keycode);
        }
        this->pressed_keys_[pos] = false;
        progress = true;
      }

      if (active != this->active_keys_[pos]) {
        this->active_keys_[pos] = active;
        if (!active) {
          pos++;
          continue;
        }
        this->active_start_[pos] = now;
      }

      if ((this->pressed_keys_[pos] == active) ||
          (now - this->active_start_[pos] < this->debounce_time_)) {
        pos++;
        continue;
      }

      ESP_LOGD(TAG, "key @ row %d, col %d pressed", key_row, key_col);
      for (auto &listener : this->listeners_)
        listener->button_pressed(key_row, key_col);
      if (!this->keys_.empty()) {
        uint8_t keycode = this->keys_[pos];
        ESP_LOGD(TAG, "key '%c' pressed", keycode);
        for (auto &listener : this->listeners_)
          listener->key_pressed(keycode);
      }
      this->pressed_keys_[pos] = true;
      progress = true;
      pos++;
    }
    row->digital_write(!has_pulldowns_);
  }

  if (!progress || this->keys_.empty())
    return;

  std::string str = "";
  for (int i = 0; i < this->columns_.size() * this->rows_.size(); i++) {
    if (this->pressed_keys_[i])
      str.push_back(this->keys_[i]);
  }
  this->progress_trigger_->trigger(str);
}

void ToggleMatrixKeypad::dump_config() {
  ESP_LOGCONFIG(TAG, "Toggle Matrix Keypad:");
  ESP_LOGCONFIG(TAG, " Rows:");
  for (auto &pin : this->rows_) {
    LOG_PIN("  Pin: ", pin);
  }
  ESP_LOGCONFIG(TAG, " Cols:");
  for (auto &pin : this->columns_) {
    LOG_PIN("  Pin: ", pin);
  }
}

void ToggleMatrixKeypad::register_listener(ToggleMatrixKeypadListener *listener) { this->listeners_.push_back(listener); }

}  // namespace toggle_matrix_keypad
}  // namespace esphome