import esphome.codegen as cg
import esphome.config_validation as cv
from esphome import pins, automation
from esphome.const import CONF_ID, CONF_PIN

CODEOWNERS = ["@zurb0"]

MULTI_CONF = True

toggle_matrix_keypad_ns = cg.esphome_ns.namespace("toggle_matrix_keypad")
ToggleMatrixKeypad = toggle_matrix_keypad_ns.class_(
    "ToggleMatrixKeypad", cg.Component
)

CONF_KEYPAD_ID = "keypad_id"
CONF_ROWS = "rows"
CONF_COLUMNS = "columns"
CONF_KEYS = "keys"
CONF_DEBOUNCE_TIME = "debounce_time"
CONF_HAS_PULLDOWNS = "has_pulldowns"
CONF_ON_PROGRESS = "on_progress"
CONF_LONG_WIRE = "long_wire"

def check_keys(obj):
    if CONF_KEYS in obj:
        if len(obj[CONF_KEYS]) != len(obj[CONF_ROWS]) * len(obj[CONF_COLUMNS]):
            raise cv.Invalid("The number of key codes must equal the number of buttons")
    return obj


CONFIG_SCHEMA = cv.All(
    cv.COMPONENT_SCHEMA.extend(
        {
            cv.GenerateID(): cv.declare_id(ToggleMatrixKeypad),
            cv.Required(CONF_ROWS): cv.All(
                cv.ensure_list({cv.Required(CONF_PIN): pins.gpio_output_pin_schema}),
                cv.Length(min=1),
            ),
            cv.Required(CONF_COLUMNS): cv.All(
                cv.ensure_list({cv.Required(CONF_PIN): pins.gpio_input_pin_schema}),
                cv.Length(min=1),
            ),
            cv.Optional(CONF_KEYS): cv.string,
            cv.Optional(CONF_DEBOUNCE_TIME, default=1): cv.int_range(min=1, max=100),
            cv.Optional(CONF_HAS_PULLDOWNS): cv.boolean,
            cv.Optional(CONF_LONG_WIRE, default=False): cv.boolean,
            cv.Optional(CONF_ON_PROGRESS): automation.validate_automation(single=True),
        }
    ),
    check_keys,
)

async def to_code(config):
    var = cg.new_Pvariable(config[CONF_ID])
    await cg.register_component(var, config)
    row_pins = []
    for conf in config[CONF_ROWS]:
        pin = await cg.gpio_pin_expression(conf[CONF_PIN])
        row_pins.append(pin)
    cg.add(var.set_rows(row_pins))
    col_pins = []
    for conf in config[CONF_COLUMNS]:
        pin = await cg.gpio_pin_expression(conf[CONF_PIN])
        col_pins.append(pin)
    cg.add(var.set_columns(col_pins))
    if CONF_KEYS in config:
        cg.add(var.set_keys(config[CONF_KEYS]))
    cg.add(var.set_debounce_time(config[CONF_DEBOUNCE_TIME]))
    if CONF_HAS_PULLDOWNS in config:
        cg.add(var.set_has_pulldowns(config[CONF_HAS_PULLDOWNS]))
    cg.add(var.set_long_wire(config[CONF_LONG_WIRE]))
    if CONF_ON_PROGRESS in config:
        await automation.build_automation(
            var.get_progress_trigger(),
            [(cg.std_string, "x")],
            config[CONF_ON_PROGRESS],
        )
