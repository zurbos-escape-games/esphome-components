import esphome.codegen as cg
import esphome.config_validation as cv
from esphome.components import spi, rc522_adv
from esphome.const import CONF_ID
# from "../rc522_adv" import rc522_adv

CODEOWNERS = ["@glmnet"]
DEPENDENCIES = ["spi"]
AUTO_LOAD = ["rc522_adv"]
MULTI_CONF = True

rc522_spi_adv_ns = cg.esphome_ns.namespace("rc522_spi_adv")
RC522SpiAdv = rc522_spi_adv_ns.class_("RC522Spi", rc522_adv.RC522_ADV, spi.SPIDevice)

CONFIG_SCHEMA = cv.All(
    rc522_adv.RC522_ADV_SCHEMA.extend(
        {
            cv.GenerateID(): cv.declare_id(RC522SpiAdv),
        }
    ).extend(spi.spi_device_schema(cs_pin_required=True))
)

FINAL_VALIDATE_SCHEMA = spi.final_validate_device_schema(
    "rc522_spi_adv", require_miso=True, require_mosi=True
)

async def to_code(config):
    var = cg.new_Pvariable(config[CONF_ID])
    await rc522_adv.setup_rc522(var, config)
    await spi.register_spi_device(var, config)
