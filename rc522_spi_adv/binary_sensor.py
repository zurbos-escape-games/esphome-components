import esphome.components.rc522_adv.binary_sensor as rc522_binary_sensor

DEPENDENCIES = ["rc522_adv"]

CONFIG_SCHEMA = rc522_binary_sensor.CONFIG_SCHEMA


async def to_code(config):
    await rc522_binary_sensor.to_code(config)
